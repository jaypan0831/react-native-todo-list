import React, { useState } from 'react';
import shortid from 'shortid';
import {
  View, FlatList, StyleSheet, Button,
} from 'react-native';

import GoalInput from './components/GoalInput';
import GoalItem from './components/GoalItem';

const styles = StyleSheet.create({
  screen: { padding: 50 },
});

export default function App() {
  const [courseGoals, setCouresGoals] = useState([]);
  const [isAddMode, setIsAddMode] = useState(false);

  const saveGoalHandler = (goalTitle) => {
    setIsAddMode(false);
    setCouresGoals((currentGoals) => [
      ...currentGoals,
      { goal: goalTitle, id: shortid.generate() },
    ]);
  };

  const removeGoalHandler = (removeId) => {
    setCouresGoals((currentGoals) => currentGoals.filter(({ id }) => id !== removeId));
  };

  const cancelGoalAdditionHandler = () => {
    setIsAddMode(false);
  };

  return (
    <View style={styles.screen}>
      <Button title='Add new Goal' onPress={() => setIsAddMode(true)} />
      <GoalInput visible={isAddMode} onAddGoal={saveGoalHandler} onCancel={cancelGoalAdditionHandler} />
      <FlatList
        keyExtractor={(item) => item.id}
        data={courseGoals}
        renderItem={({ item }) => (
          <GoalItem
            title={item.goal}
            id={item.id}
            onDelete={removeGoalHandler}
          />
        )}
      />
    </View>
  );
}

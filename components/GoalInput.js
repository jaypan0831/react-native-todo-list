import React, { useState } from 'react';
import PropTypes from 'prop-types';
import {
  View, TextInput, Button, StyleSheet, Modal,
} from 'react-native';

const styles = StyleSheet.create({
  inputContainer: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center',
  },
  input: {
    width: '80%',
    borderColor: '#a2999e',
    borderWidth: 1,
    padding: 10,
    marginBottom: 10,
  },
  ButtonContainer: {
    flexDirection: 'row',
    justifyContent: 'space-around',
    width: '60%',
  },
});

const GoalInput = ({ onAddGoal, onCancel, visible }) => {
  const [enteredGoal, setEnteredGoal] = useState('');

  const goalInputHandler = (enteredText) => {
    setEnteredGoal(enteredText);
  };

  const saveGoalHandler = () => {
    if (enteredGoal.trim().length > 0) {
      setEnteredGoal('');
      onAddGoal(enteredGoal);
    }
  };

  return (
    <Modal visible={visible} animationType='slide'>
      <View style={styles.inputContainer}>
        <TextInput
          title='ADD'
          placeholder='goal'
          onChangeText={goalInputHandler}
          style={styles.input}
          value={enteredGoal}
        />
        <View style={styles.ButtonContainer}>
          <Button title='CANCEL' color='red' onPress={onCancel} />
          <Button title='ADD' onPress={saveGoalHandler} />
        </View>
      </View>
    </Modal>
  );
};

GoalInput.propTypes = {
  onAddGoal: PropTypes.func.isRequired,
  onCancel: PropTypes.func.isRequired,
  visible: PropTypes.bool.isRequired,
};

export default GoalInput;

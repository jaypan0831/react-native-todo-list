import React from 'react';
import {
  View, Text, StyleSheet, TouchableOpacity,
} from 'react-native';
import PropTypes from 'prop-types';

const styles = StyleSheet.create({
  listItem: {
    padding: 10,
    marginVertical: 5,
    backgroundColor: '#f7ede2',
    borderColor: '#a2999e',
    borderWidth: 1,
  },
});


const GoalItem = ({ title, onDelete, id }) => (
  // Try using TouchableHighlight,
  // TouchableNativeFeedback (Only for andriod),
  // TouchableWithoutFeedback
  <TouchableOpacity onPress={() => onDelete(id)} activeOpacity={0.8}>
    <View style={styles.listItem}>
      <Text>{title}</Text>
    </View>
  </TouchableOpacity>
);

GoalItem.propTypes = {
  title: PropTypes.string.isRequired,
  id: PropTypes.string.isRequired,
  onDelete: PropTypes.func.isRequired,
};

export default GoalItem;
